import javax.jms.Destination;
import javax.jms.Queue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import javax.jms.MessageProducer;
import java.io.File;
import javax.jms.TextMessage;
import javax.jms.Message;
import com.ibm.mq.jms.MQQueueReceiver;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueSession;
import com.ibm.mq.jms.MQQueueConnection;
import java.util.Properties;

// 
// Decompiled by Procyon v0.5.36
// 

public class sampleTest
{
    Properties prop;
    MQQueueConnection connection;
    MQQueueSession qs;
    MQQueue q;
    MQQueueReceiver qrcv;
    Message msg;
    TextMessage tmsg;
    File pickupFolder;
    MessageProducer messageProducer;
    
    public sampleTest() {
        this.prop = new Properties();
        this.connection = null;
        this.qs = null;
        this.q = null;
        this.qrcv = null;
        this.msg = null;
        this.tmsg = null;
        this.pickupFolder = null;
        this.messageProducer = null;
    }
    
    public static void main(final String[] args) throws Exception {
        final sampleTest mqR = new sampleTest();
        final String propfile = "D:\\workspace\\CartersPerformance\\src\\mqSender.properties";
        mqR.doWork(propfile);
    }
    
    private void doWork(final String propfile) throws Exception {
        this.jmsinit(propfile);
        final String pickXML = this.prop.getProperty("XMLS");
        System.out.println(pickXML);
        this.pickupFolder = new File(pickXML);
        System.out.println("Output" + this.pickupFolder);
        final File[] files = this.pickupFolder.listFiles();
        final int noOfFiles = files.length;
        System.out.println("Number Of Files: " + noOfFiles);
        for (int i = 0; i < noOfFiles; ++i) {
            final StringBuilder fileData = new StringBuilder(1000);
            final BufferedReader reader = new BufferedReader(new FileReader(files[i]));
            final char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                fileData.append(buf, 0, numRead);
            }
            reader.close();
            final TextMessage txtMsg = this.qs.createTextMessage(fileData.toString());
            this.messageProducer.send((Message)txtMsg);
            System.out.println("Sent the message successfully");
        }
    }
    
    private void loadProperties(final String propname) throws IOException, FileNotFoundException {
        this.prop.load(new FileInputStream(propname));
    }
    
    private void jmsinit(final String propfile) throws Exception {
        this.loadProperties(propfile);
        System.out.println("Properties Location" + propfile);
        final MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
        final String hostName = this.prop.getProperty("HostName");
        final int portid = 41415;
        final String queueManager = this.prop.getProperty("QueueManager");
        final String channelName = this.prop.getProperty("Channel");
        final String jmsDestination = this.prop.getProperty("jmsDestination");
        System.out.println("Host Name=" + hostName);
        System.out.println("Queue Manager=" + queueManager);
        System.out.println("jmsDestination=" + jmsDestination);
        cf.setHostName(hostName);
        cf.setPort(portid);
        cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
        cf.setQueueManager(queueManager);
        cf.setChannel(channelName);
        this.connection = (MQQueueConnection)cf.createQueueConnection();
        System.out.println("Creating Connection");
        System.out.println("Creating Session");
        this.qs = (MQQueueSession)this.connection.createQueueSession(false, 1);
        System.out.println("Connecting Destination");
        this.q = (MQQueue)this.qs.createQueue(jmsDestination);
        this.qrcv = (MQQueueReceiver)this.qs.createReceiver((Queue)this.q);
        this.messageProducer = this.qs.createProducer((Destination)this.q);
        this.connection.start();
    }
    
    private void jmsend() throws Exception {
        this.qs.close();
        this.connection.close();
    }
}