import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnection;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.mq.jms.MQQueueReceiver;
import com.ibm.mq.jms.MQQueueSession;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MqSender {

	Properties prop = new Properties();
	MQQueueConnection connection = null;
	MQQueueSession qs = null;
	MQQueue q = null;
	MQQueueReceiver qrcv = null;
	Message msg = null;
	TextMessage tmsg = null;
	File pickupFolder = null;
	MessageProducer messageProducer = null;

	public static void main(String[] args) throws Exception {
		MqSender mqR = new MqSender();
		String propfile = "D:\\workspace\\CartersPerformance\\src\\mqSender.properties";
		// LoadProperties(propertiesfile);
		mqR.doWork(propfile);
	}

	private void doWork(String propfile) throws Exception {

		jmsinit(propfile);

		String pickXML = prop.getProperty("XMLS");
		// String pickXML =
		// "C:\\Users\\keshava\\Desktop\\carters\\perlscripts\\sendfiles\\XMLS\\";
		System.out.println(pickXML);
		pickupFolder = new File(pickXML);
		System.out.println("Output" + pickupFolder);

		// Get array of files present under the pickup folder
		File[] files = pickupFolder.listFiles();
		int noOfFiles = files.length;
		System.out.println("Number Of Files: " + noOfFiles);

		for (int i = 0; i < noOfFiles; i++) {

			// create the TextMessage object.
			StringBuilder fileData = new StringBuilder(1000);
			BufferedReader reader = new BufferedReader(new FileReader(files[i]));
			char[] buf = new char[1024]; // Accessing size of data byte by byte
			int numRead = 0;
			while ((numRead = reader.read(buf)) != -1) {
				fileData.append(buf, 0, numRead);
			}

			reader.close();
			TextMessage txtMsg = qs.createTextMessage(fileData.toString());

			// send the message now using messageProducer object
			messageProducer.send(txtMsg);
			System.out.println("Sent the message successfully");

			// jmsend();
		}
	}

	private void loadProperties(String propname) throws IOException, FileNotFoundException {
		prop.load(new FileInputStream(propname));
	}

	/**
	 * @param propfile
	 * @throws Exception
	 */
	private void jmsinit(String propfile) throws Exception {
		// loadProperties("C:\\Users\\keshava\\Desktop\\eclipse\\work\\sterling\\src\\mqSender.properties");
		loadProperties(propfile);
		MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
		String hostName = prop.getProperty("HostName");
		// String port=prop.getProperty("Port") ;
		// int portid=Integer.parseInt(port);
		int portid = 41415;
		String queueManager = prop.getProperty("QueueManager");
		String channelName = prop.getProperty("Channel");
		String jmsDestination = prop.getProperty("jmsDestination");
		System.out.println("Host Name=" + hostName);
		System.out.println("Queue Manager=" + queueManager);
		System.out.println("jmsDestination=" + jmsDestination);
		cf.setHostName(hostName);
		cf.setPort(portid);
		cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
		cf.setQueueManager(queueManager);
		cf.setChannel(channelName);
		connection = (MQQueueConnection) cf.createQueueConnection();
		System.out.println("Creating Connection");
		System.out.println("Creating Session");
		qs = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		System.out.println("Connecting Destination");
		q = (MQQueue) qs.createQueue(jmsDestination);
		// System.out.println("Creating Receiver") ;
		qrcv = (MQQueueReceiver) qs.createReceiver(q);
		messageProducer = qs.createProducer(q);
		connection.start();
	}

	private void jmsend() throws Exception {
		// System.out.println("Closing Session") ;
		qs.close();
		// System.out.println("Closing Connection") ;
		connection.close();
	}

}
